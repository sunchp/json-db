package com.sunchp.jsondb;

import org.junit.Test;

import java.util.List;
import java.util.Set;

public class JsonDBTemplateTest {
    @Test
    public void test() {
        JsonDBTemplate template = new JsonDBTemplate("sun demo.json");

        Set<String> collectionNames = template.getCollectionNames();
        System.out.println(collectionNames);

        System.out.println(template.collectionExists("x0"));

        List<String> stringList = template.findAll("x0", String.class);
        System.out.println(stringList);

        System.out.println(template.findByIndex(0, "x0", String.class));
    }
}