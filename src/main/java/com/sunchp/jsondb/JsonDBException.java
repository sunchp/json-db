package com.sunchp.jsondb;

public class JsonDBException extends RuntimeException {
    public JsonDBException(String message) {
        super(message);
    }

    public JsonDBException(String message, Throwable cause) {
        super(message, cause);
    }
}
