package com.sunchp.jsondb;

import com.alibaba.fastjson.JSONArray;

import java.util.List;

public class CollectionMetaData {
    private String collectionName;
    private JSONArray documents;

    public CollectionMetaData(String collectionName, JSONArray documents) {
        this.collectionName = collectionName;
        this.documents = documents;
    }

    public String getCollectionName() {
        return collectionName;
    }

    public <T> List<T> toJavaList(Class<T> entityClass) {
        return documents.toJavaList(entityClass);
    }

    public <T> T get(int index, Class<T> entityClass) {
        return documents.getObject(index, entityClass);
    }

    @Override
    public String toString() {
        return "CollectionMetaData{" +
                "collectionName='" + collectionName + '\'' +
                ", documents=" + documents +
                '}';
    }
}
